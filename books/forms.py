from django import forms

class search_form(forms.Form):
    attrs = {
        'type':'text',
        'class': 'form-control bg-secondary text-light',
        'id':'search-queries',
        'placeholder': 'This Book Loves You'
    }

    text_input = forms.CharField(label='', required=False, widget=forms.TextInput(attrs=attrs))