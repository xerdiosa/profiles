from django.test import TestCase, Client

from time import sleep

import json

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class page_test(TestCase):
    def test_books_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('books.html')
    
    def test_404(self):
        response = Client().get('bogus-page/')
        self.assertEqual(response.status_code, 404)

class functional_test(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(self):
        super().setUpClass()
        options  = Options()
        options.add_argument('--dns-prefetch-disable')
        options.add_argument('--no-sandbox')
        options.add_argument('disable-gpu')
        options.add_argument('--disable-dev-shm-usage')
        options.headless = True
        try:
            self.selenium = webdriver.Chrome(options=options)            
        except:
            self.selenium = webdriver.Chrome('./chromedriver', options=options)
        super(functional_test, self).setUpClass()
        self.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(self):
        self.selenium.quit()
        super().tearDownClass()

    def test_book_queries_functional(self):
        self.selenium.get(self.live_server_url+'/books/')
        sleep(15)

        search_queries=self.selenium.find_element_by_id('search-queries')
        search_queries.send_keys('Harry Potter')
        self.selenium.find_element_by_id('search').click()
        sleep(15)
        response = self.selenium.find_element_by_id('book-list').get_attribute('innerHTML').lower()
        self.assertIn('Harry Potter'.lower(), response)

        search_queries.clear()
        search_queries.send_keys('WorldEnd')
        self.selenium.find_element_by_id('search').click()

        sleep(15)
        response = self.selenium.find_element_by_id('book-list').get_attribute('innerHTML').lower()
        self.assertNotIn('Harry Potter'.lower(), response)
        self.assertIn('WorldEnd'.lower(), response)

