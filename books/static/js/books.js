$(document).ready(function(){
    $('#search').click(function(){
        search()
    });

    $('#search-queries').keydown(function(event){ 
        var keyCode = (event.keyCode ? event.keyCode : event.which);   
        if (keyCode == 13) {
            search();
        }
    });
})

function search(){
    changeCSS();
    getBook();
}

function changeCSS() {
    $('.margin-resp').animate({'margin-top': '1%'}, 'slow');
}

function getBook() {
    var book = $("#search-queries").val();

    if (typeof book === 'undefined' || book == '') {
        book = 'This Book Loves You'
    }

    var complete_text = '';

    $.ajax({
        url:'/books/query/',
        data:{
            'key':book
        },
        success:function (result) {
            if ($.trim($("#book-list").html()).length != 0 ) {
                $("#book-list").empty();
            }
            if (result.totalItems==0){
                $('.margin-resp').animate({'margin-top': '12%'}, 'slow');
                $("#book-list").html("<h3>Book not found!</h3>");
            }
            var complete_text = ""
            for (i = 0; i < result.items.length; i++) {
                var book_title = result.items[i].volumeInfo.title;
                var book_link = result.items[i].volumeInfo.infoLink;
                var book_thumbnail = result.items[i].volumeInfo.imageLinks;
                var book_authors = result.items[i].volumeInfo.authors;
                var book_publisher = result.items[i].volumeInfo.publisher;
                var book_alt_thumbnail = 'https://via.placeholder.com/128x200.jpg?text=image+not+found'
                var book_description = result.items[i].volumeInfo.description;

                if (typeof book_title === 'undefined') {
                    book_title = 'unknown title';
                }

                if (typeof book_link === 'undefined') {
                    book_link = '#';
                }

                if (typeof book_thumbnail === 'undefined') {
                    book_thumbnail = '#'
                } else {
                    book_thumbnail = book_thumbnail.thumbnail;
                }

                if (typeof book_publisher === 'undefined') {
                    book_publisher = 'unknown publisher';
                }

                if (typeof book_authors === 'undefined') {
                    book_authors = 'unknown author';
                } else {
                    book_authors = book_authors.join(' - ');
                }

                if (typeof book_description === 'undefined') {
                    book_description = "no description";
                }

                complete_text += '<div class="border">'+ 
                '<a href="' +book_link+'"><h4>'+ book_title + '</h4>'+
                '<img src="' + book_thumbnail+ '" alt="'+ '<img src="' +book_alt_thumbnail + '">'+ '"></a>' + 
                '<h5> Author: ' + book_authors + '</h5>' +
                '<h5> Publisher: ' + book_publisher + '</h5>' +
                '<h5 class="margin-samping"> Description:<br>' + book_description + '</h5>' +
                '</div>';
            }

            $("#book-list").append('<h3 class="margin-bawah">results for "' + book +'"</h3>'+complete_text)
        }
    })
}
//