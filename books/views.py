from django.shortcuts import render
from django.http import JsonResponse
from .forms import search_form
import requests

def books_index(request):
    response = {}
    response["form"] = search_form
    return render(request, "books_index.html", response)

def books_query(request):
    book_input = request.GET.get('key', None)
    if book_input == None:
        book_input=''

    url = 'https://www.googleapis.com/books/v1/volumes?q='+book_input
    result = requests.get(url).json()

    return JsonResponse(result)
