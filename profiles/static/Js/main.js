$(document).ready(function() {
    
    $("#change_theme_button").click(function() {
        changeTheme();
    })

    $('.accordion-header').click(function() {
        toggleAccordion(this);
    })

});

function changeTheme() {
    var current_theme = $('#theme').attr('href');
    var dark_theme = '/static/css/bootstrap_dark.min.css'
    var light_theme = '/static/css/bootstrap_light.min.css'

    if (current_theme==dark_theme){
        $('#theme').attr('href', light_theme);
    } else {
        $('#theme').attr('href', dark_theme);
    }
}

function toggleAccordion(context){
    var accordion_is_up = $(context).next().is(":hidden");
    $('.accordion-header').next().slideUp();
    if (accordion_is_up){
        $(context).next().slideDown();
    }
}