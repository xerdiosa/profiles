from django.shortcuts import render
from .forms import status_form
from .models import status

def index(request):
    response = {}
    response["form"] = status_form
    response["status"] = status.objects.all().values()
    response["status_length"] = response["status"].count()
    return render(request, "index.html", response)

def add_status(request):
    response = {}
    response['add_status'] = ""
    response['callback'] = request.META.setdefault('HTTP_REFERER', 'bogus.url')
    if request.method == "POST":
        form = status_form(request.POST)
        if form.is_valid():
            curr_status = form.cleaned_data["status"]
            post_status = status(status = curr_status)
            post_status.save()
            response['add_status'] = "penambahan status berhasil"
        else:
            response['add_status'] = "penambahan status gagal"
    else:
        response['add_status'] = "penambahan status gagal"
    return render(request, "add_status.html", response)