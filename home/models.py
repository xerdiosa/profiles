from django.db import models
from django.utils import timezone


class status(models.Model):
    status = models.CharField(max_length=50)
    posting_time = models.DateTimeField(default=timezone.now)
