from django import forms

class status_form(forms.Form):
    status_attrs = {
        'type':'text',
        'class': 'form-control bg-secondary',
        'placeholder': 'Im happy'
    }
    status = forms.CharField(label="status anda saat ini", max_length=50, required=True, widget=forms.TextInput(status_attrs))