from django.test import TestCase, Client
from .models import status

from time import sleep

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class page_test(TestCase):
    def test_index_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('index.html')
        
    
    def test_404(self):
        response = Client().get('/bogus-page/')
        self.assertEqual(response.status_code, 404)

    def test_add_status_exist(self):
        response = Client().get('/post/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('add_status.html')

    def test_valid_post(self):
        response = Client().post('/post/', data={"status": "sad"})
        self.assertEqual(response.status_code, 200)
        self.assertIn("penambahan status berhasil", response.content.decode())
        self.assertTemplateUsed('add_status.html')

    def test_invalid_post(self):
        response = Client().post('/post/', data={"status": "bogus"*11})
        self.assertEqual(response.status_code, 200)
        self.assertIn("penambahan status gagal", response.content.decode())
        self.assertTemplateUsed('add_status.html')

class functional_test(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(self):
        super().setUpClass()
        options  = Options()
        options.add_argument('--dns-prefetch-disable')
        options.add_argument('--no-sandbox')
        options.add_argument('disable-gpu')
        options.add_argument('--disable-dev-shm-usage')
        options.headless = True
        try:
            self.selenium = webdriver.Chrome(options=options)
        except:
            self.selenium = webdriver.Chrome('./chromedriver', options=options)
        super(functional_test, self).setUpClass()
        self.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(self):
        self.selenium.quit()
        super().tearDownClass()

    def test_input_form(self):
        self.selenium.get(self.live_server_url)

        self.assertIn('belum ada status :(', self.selenium.page_source)

        input_form = self.selenium.find_element_by_id('id_status')
        input_form.send_keys('bogus status')
        self.selenium.find_element_by_id('add').click()
        sleep(5)
        self.assertIn('bogus status', self.selenium.page_source)
        self.assertNotIn('belum ada status :(', self.selenium.page_source)


