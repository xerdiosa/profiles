from django.test import TestCase, Client

from time import sleep

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class profile_page_test(TestCase):
    def test_profile_page_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('profile.html')       
        
    def test_404(self):
        response = Client().get('/bogus-page/')
        self.assertEqual(response.status_code, 404)

class functional_test(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(self):
        super().setUpClass()
        options  = Options()
        options.add_argument('--dns-prefetch-disable')
        options.add_argument('--no-sandbox')
        options.add_argument('disable-gpu')
        options.add_argument('--disable-dev-shm-usage')
        options.headless = True
        try:
            self.selenium = webdriver.Chrome(options=options)            
        except:
            self.selenium = webdriver.Chrome('./chromedriver', options=options)
        super(functional_test, self).setUpClass()
        self.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(self):
        self.selenium.quit()
        super().tearDownClass()

    def test_theme(self):
        self.selenium.get(self.live_server_url+'/profile/')

        light_theme = self.live_server_url+'/static/css/bootstrap_light.min.css'
        dark_theme = self.live_server_url+'/static/css/bootstrap_dark.min.css'

        css_selector = self.selenium.find_element_by_id("theme")

        current_theme = css_selector.get_attribute('href')
        self.assertEqual(current_theme, dark_theme)
        sleep(3)

        self.selenium.find_element_by_id('change_theme_button').click()
        sleep(3)

        current_theme = css_selector.get_attribute('href')
        self.assertEqual(current_theme, light_theme)
        sleep(3)

    def test_accordion(self):
        self.selenium.get(self.live_server_url+'/profile/')

        content = self.selenium.find_element_by_class_name("accordion-content")
        display = content.value_of_css_property("display")
        self.assertEqual("none", display)

        self.selenium.find_element_by_class_name("accordion-header").click()
        sleep(3)
        content = self.selenium.find_element_by_class_name("accordion-content")
        display = content.value_of_css_property("display")
        self.assertEqual("block", display)

        self.selenium.find_element_by_class_name("accordion-header").click()
        sleep(3)
        content = self.selenium.find_element_by_class_name("accordion-content")
        display = content.value_of_css_property("display")
        self.assertEqual("none", display)


